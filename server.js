
var http = require("http")
var express = require("express");
var socketIo = require("socket.io");
var ejs = require("ejs");

var Player = require('./src/model/Player.js')
var Game = require('./src/model/Game.js')


const app = express()

const server = http.Server(app).listen(8080)
const io = socketIo(server)
const clients = {} //recebe as requisições via socket

app.use(express.static('./public'))
app.use('/vendor',express.static('./node_modules'))


app.set("views", "./public")
app.set("view engine", "html")
app.engine("html", ejs.renderFile)


app.get("/", (req, res) => {
    return res.render("index.html")
})

const games = {}
let unmatched = null; //game with just one player

io.on("connection", (socket) => {
    let id = socket.id
    console.log("New client connected. ID -> " + id)
    clients[id] = socket

    socket.on("game.begin", (data) => {
        const game = join(socket, data)
        if(game.player2){
            console.log("New game started,  " + game)

            clients[games.player1.socketId].emit("game.begin", game) // emit an event to front saying to start the game to player 1 client to server (emit)
            clients[games.player2.socketId].emit("game.begin", game) // the same with the player 2

        }
    })

    socket.on("disconnect", () => {
        console.log("client disconnect. ID -> " + id)
        delete clients[id]
    })
})

const join = (socket,data) => {
    const player = new Player(data.playerName, "X", socket.id)

    if(unmatched) {
        unmatched.player2 = player
        games[unmatched.player1.socketId] = unmatched
        games[unmatched.player2.socketId] = unmatched
        unmatched = null
        return games[socketId]
    }
    else{
        unmatched = new Game(player)
        return unmatched
    }
}